![Build Status](https://gitlab.com/pages/mkdocs/badges/master/build.svg)

---

# WP-Translations Team MkDocs

A set of docs to explain how we work and make it as simple as possible for new team member to join in.
  
## WP-Translations

WP-Translations is the first brick of a new WordPress Translation Ecosystem Services.  
It’s dedicated to WordPress developers and their Premium projects, but also to all developers with projects hosted on [translate.wordpress.org] and ready to use our services.

And we’re glad to have you getting ready to be part of its story.

[translate.wordpress.org]: https://translate.wordpress.org
