# Ready to join in and being part of the WP-Translations Team!

It's simple, just follow this step:

* Complete this [form] for general and invoice's info.

All your info are GPDR complient, we only use them for our personnal uses and will never been shared with other third parties.

And now? Give us a few minutes to setup everything for you and:

* You gonna receive a confirmation email to join our WP-Translations Team in TranslationsPress.
* Just click on it,
* You now have access to our [WP-Translations Team] projects.

* You now have access to our [Slack] workplace.

* You gonna receive a login email for our Matrix site.
* When registered, your login/password will be the same for all our Matrix sites, so make sure to keep it in a safe place.

[form]: https://goo.gl/forms/TzPslm9XV6NoyTek1
[Slack]: https://wp-translations.slack.com/
[WP-Translations Team]: https://translationspress.com/