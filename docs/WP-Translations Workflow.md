# WP-Translations workflow

Let's explain in details our workflow for each new projects:

## Pre-project

* Define by the client: locales to translate the project into.  
* Define and checked with the client: number of words and deadline.

## Project initialization

* We create a dedicated Slack channel for the project and all concerned translators/reviewers are invited to join it.
* We create a dedicated TranslationsPress project and all concerned translators/reviewers are invited to join it.  
* We create a dedicated sandbox/matrix site and all concerned translators/reviewers are invited to join for testing their work.  
* We add a tab in the invoice sheet with updated with word count, amount and deadline.  

If you aren’t available for the job just let us know.  
For teams with more than 2 members or with undefined roles make sure to define with your teammate who’s translating and who’s reviewing and let us know.

## Code audit (optional)

* We complete a full code audit of the product.
We make sure it is really i18n ready.
* We create an up to date resource file and we upload it on TranslationsPress for [strings review](#strings-review) step.

## Translation Memory
At first import on TranslationsPress to reduce the number of words to translate and improve the translations quality and consistency, our Translation Memory is applied on each new projects.  
This TM is only filled with previous translations provided by the WP-Team members and WordPress Core files.

## Strings reviews

* A native English speaker makes a full review* of all remaining strings in TranslationsPress based on [code audit](#code-audit-optional) resource file.
* All issues are reported and fixed to and by the WordPress developer project owner.
* We update the resource file on TranslationsPress and ready for the [translations](#translations) step.

## Translations

* 1 Translator/Locale.
* Invoice basis: $0.06/word.
* All translators translate on TranslationsPress.
* I10n issues are directly reported on TranslationsPress.
* i18n issues are reported only if we performed a [code audit](#code-audit-optional).
* Slack channels remain a good way to communicate between each Team members about any issues, questions we might have to create the best consistent translations possible across all locales.  
Note: String reviews are first improvements, not final ones, your feedback are always expected and welcome.
    
## Reviews

* 1 Reviewer/Locale.
* Invoice basis: $0.05/word.  
* Reviews are done in TranslationsPress
* Then they are check in our dedicated Matrix sandbox site to make sure they work as expected.  
* l10n issues are directly reported in TranslationsPress (soon).
* i18n issues are reported only if we performed a [code audit](#code-audit-optional).  
* Slack channels remain a good way to communicate between each Team members about any issues, questions we might have to create the best consistent translations possible across all locales. 
    
## Deliveries

* For Premium projects nothing to do on your side, everything is manages via the language pack feature of TranslationsPress.  
* For projects on [translate.wordpress.org] we ask CLPTE access to push all translations there.
* After we have push all translations, all GTEs are in charge of approving all WP-Translations reviewed translations in waiting state for their locale.

## Invoices

* Each month we put the necessaries project in « Invoicing  » mode in our sheet. 
* Based on those infos, you send us your invoice at invoices@wp-translations.pro.  
* Based on your preferences, we'll pay your invoice. 

## Payments

* For a practical and fee aspect of payments, we prefer to use [Transferwise] services and regular bank transfers.
* But we also have PayPal or Payoneer accounts if requested.

[translate.wordpress.org]: https://translate.wordpress.org
[Transferwise]: transferwise.com/u/francois-xavierb11