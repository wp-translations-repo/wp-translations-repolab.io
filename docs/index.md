# Welcome to WP-Translations Team MkDocs

We’ve put this docs together to explain how we work and make it as simple as possible for you to join the WP-Translations Team.

## History

WP-Translations was officially launched in May 2017 by Jérôme Sadler and I[^1].  
## WP-Translations

[WP-Translations] is the first brick of a new WordPress Translation Ecosystem Services.  
It’s dedicated to WordPress developers and their Premium projects, but also to all developers with projects hosted on [translate.wordpress.org] and ready to use our services.  

WP-Translations is also a way to give back to all polyglots and pay them for what they do best: translate WordPress plugins and themes.

## Today

25+ locales supported, 50+ WP-Team Members, +300 projects completed, happy customers...

Since June 2019, we now use our own system [TranslationsPress] for hosting, translating and reviewing all our projects. 

## What makes WP-Translations so great and why we are happy to have you in?

Our quality? Our efficiency? Our team spirit? Our i18n tools? I guess a mix of all of that, of course.  
But the most important is definitely **“We”, our translators community growing together with one objective in mind:** 

Create the First WordPress Translations Company dedicated to offer the Best i18n/l10n Services to all WordPress users. 

Are you? So let’s Translate WordPress and Spread the Good Word.

WP-T, We Provide Translations

[WP-Translations]: https//wp-translations.pro
[translate.wordpress.org]: https://translate.wordpress.org
[TranslationsPress]: https://translationspress.com

[^1]: FX Bénard
